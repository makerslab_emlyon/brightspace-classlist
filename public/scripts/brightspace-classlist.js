(() => {
    function download(filename, text) {
        const element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
    
        element.style.display = 'none';
        document.body.appendChild(element);
        element.click();
        document.body.removeChild(element);
    }
    
    const list = [];

    if (location.href.includes('print_email')) {
        document.querySelectorAll('.d2l-grid-container tr:not(.d_gh)').forEach(el => {
            const cells = [...el.cells].map(cell => cell.innerText);

            // Ignore staff
            if (cells[3] !== 'participant') return;

            const item = {
                id: parseInt(cells[2]),
                name: {
                    last: cells[0].split(',')[0].trim(),
                    first: cells[0].split(',')[1].trim()
                },
                mail: cells[1]
            };

            list.push(item);
        })
    }
    else {
        document.querySelectorAll('d2l-table-wrapper tr:not(:first-of-type)').forEach(el => {
            const cells = [...el.cells].map(cell => cell.innerText);
    
            // Ignore staff
            if (cells[5] !== 'participant') return;
    
            const item = {
                id: parseInt(cells[4]),
                name: {
                    last: cells[2].split(',')[0].trim(),
                    first: cells[2].split(',')[1].trim()
                },
                mail: cells[3]
            };
    
            list.push(item);
        });
    }


    const courseTitle = document.querySelector('.d2l-navigation-s-link').innerText;

    // Download file
    download(`${courseTitle} - classlist.json`, JSON.stringify(list, null, 2));
    alert(`${Object.keys(list).length} students found`);
})();