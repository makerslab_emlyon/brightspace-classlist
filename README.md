# Brightspace classlist generator

Simple bookmarklets to download a JSON file from a classlist on Brightspace, cause they wouldn't have a "Download as…" button, obviously.

Drag`n drop [the first bookmarklet](https://makerslab_emlyon.gitlab.io/brightspace-classlist/) to your favs (see [How to install a Bookmarklet](https://www.howtogeek.com/189358/beginner-geek-how-to-use-bookmarklets-on-any-device/)).

Then simply go to a classlist in BS and click the bookmarklet to download a JSON file.

The exported data will look like this:
```js
[
    {
        "id": 20207599,
        "name": {
            "last": "DOE",
            "first": "John"
        },
        "mail": "john.doe@edu.emlyon.com"
    },
    {
        "id": 20207013,
        "name": {
            "last": "Connor",
            "first": "Sarah"
        },
        "mail": "sarah.connor@edu.emlyon.com"
    },
    // ...
]
```

For toolboxes, we are using BS IDs instead of orgUnitID, therefore the second bookmarklet.

Bookmarklets done with https://mrcoles.com/bookmarklet/
